#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : build.sh                                                      ##
##  Project   : pw_py_termcolor                                               ##
##  Date      : Jul 09, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Vars                                                                       ##
##----------------------------------------------------------------------------##
SCRIPT_DIR="$(pw_get_script_dir)";
ROOT_DIR="$(pw_abspath "$SCRIPT_DIR"/..)";


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
set -e ## We want to stop as soon we have a error...

##
## Clean...
pw_pushd "$ROOT_DIR";
    rm -rf ./build ./dist ./*.egg-info
pw_popd;

##
## Build
pw_pushd "$ROOT_DIR";
    python3 setup.py sdist bdist_wheel
    twine check dist/*
pw_popd;

##
## Dist
pw_pushd "$ROOT_DIR";
    ## @TODO(stdmatt): Right now we are just uploading to test version of the
    ## repo since we are not sure of how things things works yet...
    twine upload --repository-url https://test.pypi.org/legacy/ dist/*
pw_popd
